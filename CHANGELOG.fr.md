[EN](CHANGELOG.md) | FR

# Changelog | liste des changements
Tous les changements notables de ce projet seront documenté dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [versionnage sémantique](https://semver.org/spec/v2.0.0.html).

## Evolutions probable / Roadmap :
- HD wallet (dérivation)
- sharding
- udid / civil_id_hash
- GVA && indexer v2s GraphQL stuff (gestion des paiements, détection des paiements...)

## [Non-publié/Non-Stabilisé] (par [1000i100])

## [Version 3.5.8] - 2023-01-12 (par [1000i100])
### Corrections
- dictionary-parser ne génère plus de doublon superflu lorsque des quantités variables sont indiquées ( comme "A{0,5}" par exemple)

## [Version 3.5.7] - 2023-01-01 (par [1000i100])
### Corrections
- dictionary-tree ne supprime plus les alternatives vides ce qui permet d'avoir le comportement attendu avec des chaines comme "(|a)".

## [Version 3.5.6] - 2023-01-01 (par [1000i100])
### Corrections
- dictionary-parser génère désormais correctement les variantes désaccentuées et majuscule/minuscule, sans couplage entre identifiant secret et mot de passe.

## [Version 3.5.5] - 2022-12-31 (par [1000i100])
### Corrections
- Dans dictionary et dictionary-parser, les différents modes autour des majuscules sont désormais strictement progressif : 0 tel quel, 1 TOUT MAJUSCULE + tout minuscule + Première Lettre En Majuscule, 2 comme 1 mais en passant en minuscule chaque caractère individuellement.

## [Version 3.5.4] - 2022-12-17 (par [1000i100])
### Corrections
- Dictionary bascule en mode sans échec (non regex) si l'interprétation du texte fourni échoue (ou si options.escapeAll=1)
- trivialDedup tri désormais les alternatives pour mieux les dédoublonner.
- dictionary-parse prend en paramètres un tableau associatif 'options' au lieu d'une liste de paramètres

- '\' est désormais considéré comme un caractère spécial
- Les sérialisations internes (rawSerialize) de Dictionary ne reconvertissent plus les caractères spéciaux

## [Version 3.5.3] - 2022-12-16 (par [1000i100])
### Corrections
- '\' est désormais considéré comme un caractère spécial
- Les sérialisations internes (rawSerialize) de Dictionary ne reconvertissent plus les caractères spéciaux

## [Version 3.5.2] - 2022-12-16 (par [1000i100])
### Corrections
- Dictionary échappe correctement les caractères spéciaux
- Dictionary splitGet permet d'avoir séparément idSec et pwd sans ambiguïté liée aux caractères échappés.

## [Version 3.5.1] - 2022-12-09 (par [1000i100])
### Corrections
- Dictionary applique systématiquement les variantes passées en option.

## [Version 3.5.0] - 2022-11-27 (par [1000i100])
### Ajouté
- dictionary intègre un mécanisme de cache pour détecter les doublons. Il est désactivable au-delà d'1_000_000 pour éviter les crashs et saturation de mémoire.
- dictionary-parser gère toutes les syntaxes d'expression régulières qu'utilisaient gsper v2 + des situations plus complexes
- dictionary-parser permet de distinguer identifiant secret et mot de passe via le séparateur `@@`
- dictionary-parser permet plusieurs types de déclinaisons : sans accents, accents optionnels, sans majuscule, majuscule optionnelle, tout en majuscule, et des déclinaisons type leetSpeak.
- dictionary-tree permet de savoir combien de combinaisons sont possibles et d'itérer dessus sans avoir besoin de les pré-générer.
### Corrections
- Vérification à chaque build (et donc dans la CI) que les packets destinés à tourner dans le navigateur n'ont aucunes dépendances.
- Suppression des dépendances résiduelles.

## [Version 3.4.2] - 2022-11-20 (par [1000i100])
### Corrections
- checkKey ne complète plus automatiquement les clefs trop courtes et envoi donc l'erreur attendue pour les clefs trop courtes.

## [Version 3.4.1] - 2022-11-20 (par [1000i100])
### Corrections
- checkKey envoi désormais des erreurs nommées, utilisable pour guider les usagers.

## [Version 3.4.0] - 2022-11-15 (par [1000i100])
### Ajouté
- crypto.textEncrypt(jsonMessage, senderPrivateKey, receiverPubKey) retourne un json format cesium+ avec `jsonMessage.title` et `jsonMessage.content` chiffrés.
- textDecrypt(jsonMessage, receiverPrivateKey)) retourne en json les champs `title` et `content` déchiffrés à partir d'un message chiffré format cesium+.

## [Version 3.3.3] - 2022-11-15 (par [1000i100])
### Corrections
- les versions 3.3.x antérieur à celle-ci, cherchaient à importer la lib crypto de node depuis le navigateur c'est corrigé.

## [Version 3.3.2] - 2022-11-12 (par [1000i100])
### Corrections
- export de sha256 pour le rendre utilisable par les clients.

## [Version 3.3.1] - 2022-09-30 (par [1000i100])
### Corrections
- suppression de crypto.isPubkey(pubkey) qui ne validait qu'à partir d'une regex contrairement à crypto.isPubKey(pubKey) qui effectue les vérifications implémentées dans la v3.3.0

## [Version 3.3.0] - 2022-09-30 (par [1000i100])
### Ajouté
- crypto.isPubKey(pubKey) identique à checkKey mais retourne vrai ou faux là où checkKey throw une erreur en cas d'échec de validation.
- crypto.checkKey(pubKey) accepte désormais aussi les clefs sans checksum grace aux vérifications précises désormais effectuable sur la clef.
- crypto.checkKey(pubKey) accepte également les clefs au format binaire.
- crypto.isDuniterPubKey(b58pubKey) vérifie la conformité au Protocol Blockchain Duniter [V11](https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md#public-key) et [V12](https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#public-key)
- crypto.isEd25519PubKey(b58pubKey) vérifie que la clef désigne bien un point sur la courbe ed25519 tel que défini dans la [RFC8032 5.1.3](https://www.rfc-editor.org/rfc/rfc8032#page-11)
### Corrections
- La CI ajuste désormais les chemins d'inclusion pour construire les builds static, que ces chemins sont décrits entre simple ou double guillemet.

## [Version 3.2.0] - 2022-01-31 (par [1000i100])
### Ajouté
- crypto.checkKey(pubKeyWithChecksum)
- EXPERIMENTAL : crypto.pubKey2checksum(b58pubKey, optionalBool:b58viewDependant, optionalBool:checksumWithoutLeadingZero) ATTENTION, la syntaxe pourrait changer selon ce qui est choisi comme algo de checksum recommandé dans les spec.
### Corrections
- correction des encodage et décodage base64 désormais conforme à la RFC 4648 §4 (standard le plus répandu)

## [Version 3.1.0] - 2021-04-01 (par [1000i100] & [Hugo])
### Ajouté
- génération du [format court d'affichage de pubKey](https://forum.duniter.org/t/format-de-checksum/7616)
	```javascript
	import {pubKey2shortKey} from 'g1lib/crypto.mjs';
	const shortKey = pubKey2shortKey("Mutu112HLfUbgVy4obN9kp3MnnDGShke88wrZr2Yr41");
	// shortKey === "Mutu…Yr41:5cq"
 	```

## [Version 3.0.2] - 2020-12-10 (par [1000i100])
### Ajouté
- minification des modules g1lib
- publication automatisée sur npm à la publication de tag (sous réserve que la CI passe)

## [Version 3.0.1] - 2020-12-10 (par [1000i100])
### Ajouté
- test unitaire exécuté dans la CI
- couverture de test
- suivi de la maintenabilité / complexité
- suivi de la duplication

## [Version 3.0.0] - 2020-12-10 (par [1000i100])

### Ajouté
- module g1lib.js séparé de Gsper



## [Version 2.1.0] - 2018-06-27 (par [1000i100])

### Ajouté
- syntaxe =référence> pour faire des références syncronisées et éviter de générer des variantes non souhaitées.

## [Version 2.0.0] - 2018-05-10 (par [1000i100])

### Ajouté

##### Générateur de variantes de mot de passe
- Déclinaisons avec Majuscules
- Désaccentuation
- Déclinaison avancée façon expression régulière
##### Documentation
- Rédaction d'une documentation des générateurs de variante de mot de passe
##### Améliorations techniques
- Ajout de test unitaire (meilleure fiabilité).
- Différentiation de la lib pour la partie crypto et de celle de génération de variantes (meilleure maintenabilité et évolutivité).

## [Version 1.0.1 (Proof of Concept)] - 2018-04-18 (par [1000i100])
### Ajouté

##### Algorithme
- intégration des librairies de crypto nécessaires
- calcul de la clef publique correspondant à chaque combinaison de secrets saisie, et comparaison à la clef publique de référence.

[Non-publié/Non-Stabilisé]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.5.7...main

[Version 3.5.7]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.5.6...v3.5.7
[Version 3.5.6]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.5.5...v3.5.6
[Version 3.5.5]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.5.4...v3.5.5
[Version 3.5.4]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.5.3...v3.5.4
[Version 3.5.3]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.5.2...v3.5.3
[Version 3.5.2]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.5.1...v3.5.2
[Version 3.5.1]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.5.0...v3.5.1
[Version 3.5.0]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.4.2...v3.5.0
[Version 3.4.2]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.4.1...v3.4.2
[Version 3.4.1]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.4.0...v3.4.1
[Version 3.4.0]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.3.3...v3.4.0
[Version 3.3.3]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.3.2...v3.3.3
[Version 3.3.2]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.3.1...v3.3.2
[Version 3.3.1]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.3.0...v3.3.1
[Version 3.3.0]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.2.0...v3.3.0
[Version 3.2.0]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.1.0...v3.2.0
[Version 3.1.0]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.0.3...v3.1.0
[Version 3.0.2]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.0.1...v3.0.2
[Version 3.0.1]: https://git.duniter.org/libs/g1lib.js/-/compare/v3.0.0...v3.0.1
[Version 3.0.0]: https://git.duniter.org/libs/g1lib.js/-/compare/v2.1.0...v3.0.0
[Version 2.1.0]: https://git.duniter.org/tools/gsper/-/compare/v2.0.0...v2.1.0
[Version 2.0.0]: https://git.duniter.org/tools/gsper/-/compare/v1.0.1...v2.0.0
[Version 1.0.1 (Proof of Concept)]: https://git.duniter.org/tools/gsper/-/tree/v1.0.1

[1000i100]: https://framagit.org/1000i100 "@1000i100"
[Hugo]: https://trentesaux.fr/
