const fs = require('fs');

const covData = JSON.parse(fs.readFileSync('generated/cov-dead-code-deletion/coverage-final.json', 'utf8'));
const files = Object.keys(covData);
for (const filePath of files) {
	const fullContent = fs.readFileSync(filePath, 'utf8').split('\n');
	const keptContent = [];
	const statementMap = covData[filePath].statementMap;
	const statementRan = covData[filePath].s;
	for (const key of Object.keys(statementMap)) {
		if (statementRan[key] > 0) for (let line = statementMap[key].start.line; line <= statementMap[key].end.line; line++) {
			keptContent[line - 1] = fullContent[line - 1];
		}
	}

	const fnMap = covData[filePath].fnMap;
	const fnRan = covData[filePath].f;
	for (const key of Object.keys(fnMap)) {
		if (fnRan[key] > 0) for (let line = fnMap[key].decl.start.line; line <= fnMap[key].decl.end.line; line++) {
			keptContent[line - 1] = fullContent[line - 1];
		}
	}

	fs.writeFileSync(filePath, keptContent.join('\n'));
}
