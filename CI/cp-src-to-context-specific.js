const fs = require('fs');

const allModules = [];
fs.readdirSync('src/').forEach(fileName => {
	if (!fileName.includes('.')) return 'sub directory excluded';

	copy(`src/`, `generated/tmpBrowser/`, fileName);
	copy(`src/`, `generated/tmpNodejs/`, fileName);

	if (fileName.includes('.test')) return;
	const moduleName = fileName.split('.')[0];
	allModules.push(moduleName);
});
copy(`src/context-dependant/only-browser.mjs`, `generated/tmpBrowser/context-dependant/generics.mjs`);
copy(`src/context-dependant/only-nodejs.mjs`, `generated/tmpNodejs/context-dependant/generics.mjs`);
copy('package.json', 'generated/tmpBrowser/package.json');
copy('package.json', 'generated/tmpNodejs/package.json');

const allMjsContent = `${
	allModules.map(module => `import * as ${snake2camelCase(module)} from './${module}.mjs';`).join('\n')
}

export {${allModules.map(module => snake2camelCase(module)).join(', ')}};
`;
fs.writeFileSync(`generated/tmpBrowser/all.mjs`, allMjsContent);
fs.writeFileSync(`generated/tmpNodejs/all.mjs`, allMjsContent);

function copy(srcPath, targetPath, fileName = '') {
	let content = fs.readFileSync(`${srcPath}${fileName}`, 'utf8');
	content = content.replace(/from '\.\.\//g, 'from \'../../');
	content = content.replace(/from "\.\.\//g, 'from "../../');
	fs.writeFileSync(`${targetPath}${fileName}`, content);
	if (!fileName.includes('.test')) return;
	content = content.replace(
		/import \* as app from '\.\/([^.]+)\.mjs';/,
		(full, libName) => `import {${snake2camelCase(libName)} as app} from './all.mjs';`);
	fs.writeFileSync(`${targetPath}all.${fileName}`, content);
}

function snake2camelCase(str) {
	return str.replace(/[^a-zA-Z0-9]+/g, ' ').replace(/ \w/g, chr => chr.toUpperCase()[1]); // eslint-disable-line unicorn/better-regex
}
