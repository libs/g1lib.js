import * as duniCrypto from "../src/crypto.mjs";

// For thread use : https://github.com/darionco/WebCPU
const fillChr = '                            ';

function f(size, str) { // format
	const toFill = size - str.length;
	if (toFill > 0) return fillChr.substring(0, toFill) + str;
	return str;
}

function main() {

	let PARALLEL_INDEX = 0;
	if (process.argv[2]) PARALLEL_INDEX = parseInt(process.argv[2]);
	const PARALLEL_THREAD = 42;

	const startTime = Date.now(); // performance.now()
	let partialLevel = 0;

	for (let x = PARALLEL_INDEX; true; x += PARALLEL_THREAD) {
		const strX = ('vanityDebugSeed' + x).replace(/0/g, 'z');
		const keys = duniCrypto.raw2b58(duniCrypto.seed2keyPair(duniCrypto.b58pubKey2bin(strX)));
		let pubKey = keys.publicKey;
		if (pubKey[0] === '1') {
			pubKey = duniCrypto.b58.encode(duniCrypto.sliceInitialsZero(duniCrypto.b58pubKey2bin(pubKey)));
			const now = Date.now(); // performance.now()
			const elapsed = f(7, '' + Math.trunc((now - startTime) / 1000));
			const speed = f(5, '' + Math.trunc((x / PARALLEL_THREAD) / ((now - startTime) / 1000)));
			if (partialLevel < 2) {
				partialLevel = 1;
				console.log(`${speed}/s écoulé: ${elapsed}s seed:${f(44, strX)} pubKey:${f(44, pubKey)}`);
			}
			if (pubKey.length === 42) {
				if (partialLevel < 3) {
					partialLevel = 2;
					console.log(`${speed}/s écoulé: ${elapsed}s seed:${f(44, strX)} pubKey:${f(44, pubKey)}`);
				}
				if (duniCrypto.pubKey2checksum(pubKey)[0] === '1') {
					if (partialLevel < 4) {
						partialLevel = 3;
						console.log(`${speed}/s écoulé: ${elapsed}s seed:${f(44, strX)} pubKey:${f(44, pubKey)} chk: ${duniCrypto.pubKey2checksum(pubKey)} ${duniCrypto.pubKey2checksum(pubKey, false, true)}`);
					}
					if (duniCrypto.pubKey2checksum(pubKey, true)[0] === '1') {
						console.log(`${speed}/s écoulé: ${elapsed}s seed:${f(44, strX)} pubKey:${f(44, pubKey)
						} chk: ${duniCrypto.pubKey2checksum(pubKey)} ${duniCrypto.pubKey2checksum(pubKey, false, true)
						} ${duniCrypto.pubKey2checksum(pubKey, true)} ${duniCrypto.pubKey2checksum(pubKey, true, true)}`);
						return;
					}
				}
			}
		}
	}
}

main();

// 111111111111111111111111111194999 BswTw MRTt8 6WCPk P3g1N 3kVZx 29cPB oTPUq 8rk41 gup
// 111111111111111111111111111111451 12rxvfozNLMgzH5j8sQZUCZKSMh5NGBYqaqXiSq5ifCE 7T5 7T5 7T5 7T5

// 161/s écoulé: 105s seed:171z8 pubKey:MKZWhcbCVoxLrUyZGKTQQDJwkg3CyxDJEf1NFfUdgk
// 161/s écoulé: 105s seed:171z8 pubKey: MKZWh cbCVo xLrUy ZGKTQ QDJwk g3Cyx DJEf1 NFfUd gk
