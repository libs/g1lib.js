import {readdirSync, readFileSync, writeFileSync} from 'node:fs';
import {transform} from 'esbuild';

minFold('generated/npm/browser/');
minFold('generated/npm/nodejs/');

async function minFold(folder) {
	readdirSync(folder).forEach(async fileName => {
		if (!fileName.includes('.mjs')) return;
		const orgContent = readFileSync(folder + fileName, 'utf8').replace(/import '[^ ']+';/g,'');
		const minified = await transform(orgContent, { minify: true, treeShaking:true, target: "esnext", legalComments:'none', format: 'esm' });
		writeFileSync(folder + fileName, minified.code);
	});
}
