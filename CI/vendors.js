const fs = require('fs');
try {
	fs.writeFileSync('generated/vendors/tweetnacl+ed2curve.mjs',
		(fs.readFileSync('node_modules/tweetnacl/nacl-fast.js', 'utf8'))
			.replace('(function(nacl) {', 'var nacl = {};')
			.replace('})(typeof module !== \'undefined\' && module.exports ? module.exports : (self.nacl = self.nacl || {}));', 'export default nacl;')
		+(fs.readFileSync('node_modules/ed2curve/src/index.mjs', 'utf8'))
		.replace(/^[\s\S]+----\s+\/\/ Converts Ed25519[^\n]+/,'')
		, 'utf8');
} catch (error) {
	console.error(error);
}

try {
	fs.writeFileSync('generated/vendors/scrypt.mjs',
		(fs.readFileSync('node_modules/scrypt-async-modern/dist/index.js', 'utf8'))
			.replace('exports.default = scrypt;', 'export default scrypt;')
			.replace('Object.defineProperty(exports, "__esModule", { value: true });', '')
		, 'utf8');
} catch (error) {
	console.error(error);
}

try {
	fs.writeFileSync('generated/vendors/@noble-ed25519-node.mjs',
		(fs.readFileSync('node_modules/@noble/ed25519/lib/esm/index.js', 'utf8'))
		, 'utf8');
} catch (error) {
	console.error(error);
}
try {
	fs.writeFileSync('generated/vendors/@noble-ed25519-browser.mjs',
		(fs.readFileSync('node_modules/@noble/ed25519/lib/esm/index.js', 'utf8'))
			.replace(`import * as nodeCrypto from 'crypto';`,`let nodeCrypto;`)
		, 'utf8');
} catch (error) {
	console.error(error);
}
