import {readdirSync, readFileSync} from 'node:fs';

zeroDep('generated/npm/browser/');


async function zeroDep(folder) {
	readdirSync(folder).forEach(async fileName => {
		if (!fileName.includes('.mjs') || fileName.includes('.test.mjs') || fileName.includes('.test-e2e.mjs')) return;
		const content = readFileSync(folder + fileName, 'utf8');
		if(folder.includes('browser') && content.includes('import ')) throw new Error('Dependencies found in '+folder+fileName);//process.exit(1);
	});
}
