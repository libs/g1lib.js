import * as ed25519 from '../../generated/vendors/@noble-ed25519-browser.mjs';

const fetch = window.fetch;

async function random(u8a,bytes){
	if(!u8a && !bytes) return Math.random();
	if(!bytes && typeof u8a === "number"){
		bytes = u8a;
		u8a = new Uint8Array(bytes);
	}
	const QUOTA = 65536;
	const n = bytes;
	const v = new Uint8Array(bytes);
	for (let i = 0; i < n; i += QUOTA) crypto.getRandomValues(v.subarray(i, i + Math.min(n - i, QUOTA)));
	for (let i = 0; i < n; i++) u8a[i] = v[i];
	return u8a;
}

export {fetch, random, ed25519}
