import fetch from '../../node_modules/node-fetch/lib/index.mjs';
import * as ed25519 from '../../generated/vendors/@noble-ed25519-node.mjs';

//import fetch from '../../node_modules/node-fetch/src/index.js';
//import fetch from 'node-fetch';
//import crypto from "crypto";
export {fetch,random, ed25519};

function random(u8a,bytes){
	if(!u8a && !bytes) return Math.random();
	if(!bytes && typeof u8a === "number"){
		bytes = u8a;
		u8a = new Uint8Array(bytes); // Buffer pour les vieilles versions de node
	}
	const n = bytes;
	const v = crypto.randomBytes(n);
	for (let i = 0; i < n; i++) u8a[i] = v[i];
	return u8a;
}
