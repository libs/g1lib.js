import test from 'ava';
import * as app from './dictionary-parser.mjs';

test('parse remove ref:: lines', t => {
	t.is(app.parse('ref::truc',{idSecPwd:false}), '');
});
test('parse handle <ref>', t => {
	t.is(app.parse(`ref::truc\nref::bidule\n<ref> <ref>`,{idSecPwd:false}), '(bidule|truc) (bidule|truc)');
	t.is(app.parse(`ref::(truc|bidule)\n<ref> <ref>`,{idSecPwd:false}), '(bidule|truc) (bidule|truc)');
});
test('parse handle <ref> complex inclusion', t => {
	t.is(app.parse(`ref::a<ref2>\n<ref>\nref2::(b|c)`,{idSecPwd:false}), 'a(b|c)');
});
test('parse detect infinite recursion with <ref> and throw', t => {
	t.throws(()=>app.parse(`ref::a<ref>\n<ref>`,{idSecPwd:false}));
});
test('parse detect infinite recursion with nested <ref> and throw', t => {
	t.throws(()=>app.parse(`ref::a<ref2>\n<ref>\nref2::b<ref>`,{idSecPwd:false}));
});

test('parse handle [ -_]', t => {
	t.is(app.parse('[ -_]',{idSecPwd:false}), '( |-|_)');
});
test('parse handle [a-f]', t => {
	t.is(app.parse('[a-f]',{idSecPwd:false}), '(a|b|c|d|e|f)');
	t.is(app.parse('[7-9]',{idSecPwd:false}), '(7|8|9)');
	t.is(app.parse('[C-F]',{idSecPwd:false}), '(C|D|E|F)');
});
test('parse handle [a-c-]', t => {
	t.is(app.parse('[a-c-]',{idSecPwd:false}), '(-|a|b|c)');
});

test('parse handle {qty}', t => {
	t.is(app.parse('a{5}',{idSecPwd:false}), 'aaaaa');
});
test('parse handle {min,max}', t => {
	t.is(app.parse('b{3,5}',{idSecPwd:false}), 'bbb(|b|bb)');
});
test('parse handle (string){qty}', t => {
	t.is(app.parse(`c'est (toto|tata){0,2}`,{idSecPwd:false}), `c'est ((tata|toto)(tata|toto)||tata|toto)`);
});
test('parse handle nested (s|t(ri|ng)){qty}', t => {
	t.is(app.parse(`(s|t(ri|ng)){1,2}`,{idSecPwd:false}), `(t(ng|ri)|s)(t(ng|ri)||s)`);
});

test('parse handle plop:\\:', t => {
	t.is(app.parse('plop:\\:ici',{idSecPwd:false}), 'plop:\\:ici');
	t.is(app.parse('plop\\::ici',{idSecPwd:false}), 'plop\\::ici');
	t.is(app.parse('plop::ici',{idSecPwd:false}), '');
});
test('parse handle [\\]*]', t => {
	t.is(app.parse('[\\]*]',{idSecPwd:false}), '(*|\\])');
});

test('parse handle =ref>', t => {
	t.is(app.parse(`ref::truc\nref::bidule\n=ref> =ref>`,{idSecPwd:false}), '(bidule bidule|truc truc)');
	t.is(app.parse(`ref::(truc|bidule)\n=ref> =ref>`,{idSecPwd:false}), '(bidule bidule|truc truc)');
});
test('parse handle =ref> without generating duplication', t => {
	t.is(app.parse(`ref::(truc|bidule)\n(=ref> =ref>|machin)`,{idSecPwd:false}), '(bidule bidule|machin|truc truc)');
});
test('parse handle multiple =ref>', t => {
	t.is(
		app.parse(`=ref> =ref2> =ref> =ref2>\nref::(truc|bidule)\nref2::(machin|chose)`,{idSecPwd:false}),
		'(bidule chose bidule chose|bidule machin bidule machin|truc chose truc chose|truc machin truc machin)');
});
test('parse handle multi-level =ref>', t => {
	t.is(app.parse(`=ref> =ref> =ref2>\nref::(truc|=ref2>)\nref2::(machin|chose)`,{idSecPwd:false}),
		'(chose chose chose|machin machin machin|truc truc chose|truc truc machin)');
});
test('parse throw if unconverted =ref>', t => t.throws(()=>app.parse(`=ref>\nref::=ref>`,{idSecPwd:false})));
/*TODO: test('parse handle multi-level =ref> in all case', t => {
	t.is(app.parse(`=ref2> =ref> =ref>\nref::(truc|=ref2>)\nref2::(machin|chose)`,false),
		'(machin truc truc|chose truc truc|machin machin machin|chose chose chose)');
});*/

test('no @@ idSec password separator ? implicit add it', t => {
	t.is(app.parse('a'), 'a@@a');
});
test('@@ present, do not add it', t => {
	t.is(app.parse('a@@b'), 'a@@b');
});
test('no @@ on each line ? combine no @@ lines', t => {
	t.is(app.parse(`
e@@(f|g)
(a|b)
(c|d)
h@@(i|j)
`), '((|a|b|c|d)@@(|a|b|c|d)|e@@(f|g)|h@@(i|j))');
});
test('add @@ on part without it', t => {
	t.is(app.parse(`(a@@b|c)`), '(a@@b|c@@c)');
});
test('add @@ on part without it complex case', t => {
	t.is(app.parse(`(a(b|(c|d)@@(e|f)|g@@h|i)|(j|(k@@(l|m)n|o)))p`),
		'((a((c|d)@@(e|f)|g@@h)|k@@(l|m)n)p|(a(b|i)|j|o)p@@(a(b|i)|j|o)p)');
});
test('throw if multiple @@ in the same sequence', t => t.throws(() => app.parse(`a@@(b|c)@@d`)));

test('add no accents variant', t => t.is(app.parse('Ǧ1ǦdiT ici',{idSecPwd:false, accent:1}), '(G1GdiT ici|Ǧ1ǦdiT ici)'));
test('add optional accents variants', t => t.is(app.parse('Ǧ1ǦdiT ici',{idSecPwd:false, accent:2}), '(G|Ǧ)1(G|Ǧ)diT ici'));
test('add variant : FULL UPPERCASE, full lowercase, Capitalized Words',
		t => t.is(app.parse('Ǧ1ǦdiT ici',{idSecPwd:false, lowerCase:1}), '(Ǧ1ǦDIT ICI|Ǧ1ǦdiT Ici|Ǧ1ǦdiT ici|Ǧ1ǧdit Ici|ǧ1ǧdit ici)'));
test('add variant : FULL UPPERCASE and all partial lowercase variant from Capitalized',
		t => t.is(app.parse('Ǧ1ǦdiT ici',{idSecPwd:false, lowerCase:2}), '((Ǧ|ǧ)1(Ǧ|ǧ)di(T|t) (I|i)ci|Ǧ1ǦDIT ICI)'));
test('add no leetSpeak variants', t => t.is(app.parse('Ǧ1ǦdiT ici',{idSecPwd:false, leetSpeak:1}), 'Ǧ(1|I|L|T|i|l|t)ǦdiT ici'));
test('add leetSpeak variants', t => t.is(app.parse('Ǧ1ǦdiT ici',{idSecPwd:false, leetSpeak:2}), '(Ǧ1Ǧd1(1|7) 1c1|Ǧ1ǦdiT ici)'));
test('add every leetSpeak variants', t => t.is(app.parse('Ǧ1ǦdiT ici',{idSecPwd:false, leetSpeak:3}), 'Ǧ(1|I|L|T|i|l|t)Ǧd(1|I|L|T|i|l|t)(1|7|I|L|T|Y|i|l|t|y) (1|I|L|T|i|l|t)c(1|I|L|T|i|l|t)'));
//test('add all variants', t => t.true(app.parse('Ǧ1ǦdiT ici',{idSecPwd:true, accent:2, lowerCase:4, leetSpeak:3}).length>500));
