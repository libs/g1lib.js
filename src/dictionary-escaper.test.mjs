import test from "ava";
import * as app from "./dictionary-escaper.mjs";
import {escape2utfSpecial} from "./dictionary-escaper.mjs";

test('unescape special characters & re-escape them generate identical string', t => {
	const cases = [
		'plop:\\:ici',
		'[\\]*]',
		'\\{\\}',
		'\\\\',
		'8\\<9<',
	]
	cases.forEach(c=>t.is(app.utfSpecial2escaped(app.escape2utfSpecial(c)), c));
});
test('unescape special characters & reconvert without escaping remove escaping', t => {
	t.is(app.utfSpecial2unEscaped(app.escape2utfSpecial('plop:\\:ici')), 'plop::ici');
});
test('unescape usual characters & re-escape them generate usual string', t => {
	t.is(app.utfSpecial2escaped(app.escape2utfSpecial('\\a')), 'a');
});
test('escapeAll convert any special character of a string', t => {
	t.is(app.utfSpecial2escaped(app.escapeAll('a:[]{}()<>-=\\b')), 'a\\:\\[\\]\\{\\}\\(\\)\\<\\>\\-\\=\\\\b');
});
