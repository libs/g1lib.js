const specialMap = {
	'(': String.fromCharCode(0x2300),
	')': String.fromCharCode(0x2301),
	'|': String.fromCharCode(0x2302),
	'{': String.fromCharCode(0x2303),
	'}': String.fromCharCode(0x2304),
	',': String.fromCharCode(0x2305),
	'[': String.fromCharCode(0x2306),
	']': String.fromCharCode(0x2307),
	'-': String.fromCharCode(0x2308),
	'<': String.fromCharCode(0x2309),
	'>': String.fromCharCode(0x230a),
	':': String.fromCharCode(0x230b),
	'=': String.fromCharCode(0x230c),
	'@': String.fromCharCode(0x230d),
	'\\': String.fromCharCode(0x230e),
};
const revertSpecial = swapKeyValue(specialMap);
function swapKeyValue(object) {
	const result = {};
	for (const key in object) {
		result[object[key]] = key;
	}

	return result;
}
export function escape2utfSpecial(str) {
	return str.replace(/\\(.)/g, (a, chr) => specialMap[chr] ? specialMap[chr] : chr);
}
export function utfSpecial2unEscaped(str) {
	return str.split('').map(chr => revertSpecial[chr] ? revertSpecial[chr] : chr).join('');
}
export function utfSpecial2escaped(str) {
	return str.split('').map(chr => revertSpecial[chr] ? `\\${revertSpecial[chr]}` : chr).join('');
}
export function escapeAll(str){
	return str.replace(/./g, chr => specialMap[chr] ? specialMap[chr] : chr);
}
