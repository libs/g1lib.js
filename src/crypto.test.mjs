import test from 'ava';
import * as app from './crypto.mjs';

const idSec = 'a';
const mdp = 'b';
// Base58
const pubKey = 'AoxVA41dGL2s4ogMNdbCw3FFYjFo5FPK36LuiW1tjGbG';
const secretKey = '3ZsmZhnRv137dS1s7Q3jFGKLTDyhkwguPHfnWBxzDCTTHKWGnYw9zBk3gcCUJCc72TEUuyzM7cqpo7c5LYhs1Qtv';
const seed = '9eADqX8V6VcPdJCHCVYiE1Vnift9nFNrvr9aTaXA5RJc';
const unsignedDocument = `
Version: 10
Type: Identity
Currency: duniter_unit_test_currency
Issuer: AoxVA41dGL2s4ogMNdbCw3FFYjFo5FPK36LuiW1tjGbG
UniqueID: tic
Timestamp: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855`;
const signedDocument = `Version: 10
Type: Identity
Currency: duniter_unit_test_currency
Issuer: AoxVA41dGL2s4ogMNdbCw3FFYjFo5FPK36LuiW1tjGbG
UniqueID: tic
Timestamp: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855
8BZ2NE/d4YO2rOFpJFZdEYTIoSL4uSX9zo6tacpHBcCIlSlhkHTIHbSJNuzLl9uVBIO0skI7NZPxEYXIJGQYBg==`;

test('signDocument', async t => t.is(await app.signDocument(unsignedDocument, secretKey), signedDocument));
test('b64 sign string', async t => t.is(await app.sign(unsignedDocument, secretKey), 'G6ma6n+rpJ+7PPsUuJjNtzfGQqLWNqRToSlurt8vjrHa7G0tm7oVObJjQBWGMK0zs4/25xXidT19RrfZqWV/DQ=='));
test('b58 sign string', async t => t.is(await app.sign(unsignedDocument, secretKey, 'b58'), 'Z5W7C7ZUwTRPRTCrXtRqmY4WakgEXAkPBDiVEVxoTwrCSvDrup19sENe9tfFNMFKL9ZFdiFWJCSJ2ftgeeDzFsz'));
test('raw sign string', async t => t.is((await app.sign(unsignedDocument, secretKey, 'raw'))[0], 27));
test('array sign string', async t => t.is((await app.sign(unsignedDocument, secretKey, 'Array'))[0], 27));
test('uint8array sign string', async t => t.is((await app.sign(unsignedDocument, secretKey, 'uint8array'))[0], 27));
test('sign throw for bad output format', t => t.throws(() => app.sign(unsignedDocument, secretKey, 'whattt ?')));

//test('signOnly(message, privateKey)', t => t.is(app.signOnly('a message', secretKey), 'signature'));
//test('signOnly(message, privateKey, returnFormat=uint8array)', t => t.is(app.signOnly([0,1,2,3], secretKey), [0,1,2,3]));
//test('signDocument or signMessage(message, privateKey)', t => t.is(app.sign('a message', secretKey), 'message avec sa signature'));
//test('sign(uint8array, privateKey, returnFormat=uint8array)', async t => t.is(await app.sign([0,1,2,3], secretKey,'uint8array'), [0,0,0,0]));
//test('checkSign(message, issuerPubKey) succeed', t => t.is(app.checkSign('a message', secretKey), true));
//test('checkSign(message, issuerPubKey) fail', t => t.is(app.checkSign([0,1,2,3], secretKey,'uint8array'), false));


test('b58 should decode/encode well', t => t.is(app.b58.encode(app.b58.decode(pubKey)), pubKey));
test('b58 on pubKey with leading 1', t => t.is(app.b58.encode(app.b58.decode('12BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx')), '12BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx'));
test('b58 on pubKey without leading 1', t => t.is(app.b58.encode(app.b58.decode('2BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx')), '2BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx'));

test('saltPass2seed should convert salt & password to seed with scrypt', async t => {
	t.is(app.b58.encode(await app.saltPass2seed(idSec, mdp)), seed);
});
test('seed2keyPair should generate public and private key nacl/sodium way.', async t => {
	const rawSeed = app.b58.decode(seed);
	const rawKeyPair = await app.seed2keyPair(rawSeed);
	t.is(app.b58.encode(rawKeyPair.publicKey), pubKey);
	t.is(app.b58.encode(rawKeyPair.secretKey), secretKey);
	t.deepEqual(app.b58.decode(secretKey), rawKeyPair.secretKey);
});
test('idSecPass2cleanKeys should output clean base58 keys and seed', async t => {
	const r = await app.idSecPass2cleanKeys(idSec, mdp);
	t.is(r.publicKey, pubKey);
	t.is(r.secretKey, secretKey);
	t.is(r.seed, seed);
	t.is(r.idSec, idSec);
	t.is(r.password, mdp);
});
test('pubKey2shortKey match Mutu…Yr41:5cq', t => {
	const pubKey = 'Mutu112HLfUbgVy4obN9kp3MnnDGShke88wrZr2Yr41';
	const shortKey = 'Mutu…Yr41:5cq';
	t.is(app.pubKey2shortKey(pubKey), shortKey);
});
test('pubKey2shortKey match RML1…zvSY:3k4', t => {
	const pubKey = 'RML12butzV3xZmkWnNAmRwuepKPYvzQ4euHwhHhzvSY';
	const shortKey = 'RML1…zvSY:3k4';
	t.is(app.pubKey2shortKey(pubKey), shortKey);
});
test('pubKey2checksum RML12butz : 3k4', t => t.is(app.pubKey2checksum('RML12butzV3xZmkWnNAmRwuepKPYvzQ4euHwhHhzvSY'), '3k4'));
test('pubKey2checksum 12Bj : 8pQ', t => t.is(app.pubKey2checksum('12BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx'), '8pQ'));
test('pubKey2checksum 2Bjy : 8pQ', t => t.is(app.pubKey2checksum('2BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx'), '8pQ'));
test('pubKey2checksum ascii 2Bjy : 5vi', t => t.is(app.pubKey2checksum('2BjyvjoAf5qik7R8TKDJAHJugsX23YgJGi2LmBUv2nx', true), '5vi'));
test('pubKey2checksum 1111 : 3ud', t => t.is(app.pubKey2checksum('11111111111111111111111111111111'), '3ud'));
test('pubKey2checksum "" : 3ud', t => t.is(app.pubKey2checksum(''), '3ud'));
test('pubKey2checksum 1pubKey542 : MLT', t => t.is(app.pubKey2checksum('1pubKey542'), 'MLT'));
test('pubKey2checksum pubKey542 : MLT', t => t.is(app.pubKey2checksum('pubKey542'), 'MLT'));
test('pubKey2checksum ascii 1111111111111111111111111pubKey542 : MLT', t => t.is(app.pubKey2checksum('1111111111111111111111111pubKey542', true), 'MLT'));
test('pubKey2checksum ascii 1pubKey542 : DSs', t => t.is(app.pubKey2checksum('1pubKey542', true), 'DSs'));
test('pubKey2checksum ascii pubKey542 : DEE', t => t.is(app.pubKey2checksum('pubKey542', true), 'DEE'));
test('pubKey2checksum checksumWithLeadingZero 1pubKey542 : 1ML', t => t.is(app.pubKey2checksum('pubKey542', false, true), '1ML'));

test('checkKey pubKey542:1ML', t => t.true(app.checkKey('pubKey542:1ML', false)));
test('checkKey pubKey542:MLT', t => t.true(app.checkKey('pubKey542:MLT', false)));
test('checkKey pubKey542:DEE', t => t.true(app.checkKey('pubKey542:DEE', false)));

test('checkKey 11111111111111111111111pubKey49311:4Ru', t => t.true(app.checkKey('11111111111111111111111pubKey49311:4Ru', false)));
test('checkKey 11111111111111111111111pubKey49311:14R', t => t.true(app.checkKey('11111111111111111111111pubKey49311:14R', false)));
test('checkKey 111pubKey49311:14R', t => t.true(app.checkKey('111pubKey49311:14R', false)));
test('checkKey 11pubKey49311:14R', t => t.true(app.checkKey('11pubKey49311:14R', false)));
test('checkKey 1pubKey49311:14R', t => t.true(app.checkKey('1pubKey49311:14R', false)));
test('checkKey pubKey49311:14R', t => t.true(app.checkKey('pubKey49311:14R', false)));
test('checkKey pubKey49311:4Ru', t => t.true(app.checkKey('pubKey49311:4Ru', false)));
test('checkKey pubKey49311:12p', t => t.true(app.checkKey('pubKey49311:12p', false)));
test('checkKey pubKey49311:2p7', t => t.true(app.checkKey('pubKey49311:2p7', false)));
test('checkKey false 11111111111111111111111pubKey49311:12p', t => t.throws(() => app.checkKey('11111111111111111111111pubKey49311:12p', false)));
test('checkKey false 11111111111111111111111pubKey49311:2p7', t => t.throws(() => app.checkKey('11111111111111111111111pubKey49311:2p7', false)));
test('checkKey false pubKey49311:111', t => t.throws(() => app.checkKey('pubKey49311:111', false)));

test('checkKey false 0pubKey49311:any', t => t.throws(() => app.checkKey('0pubKey49311:any', false)));

test('pubKey2checksum 11111111111111111111111pubKey49311 : 4Ru', t => t.is(app.pubKey2checksum('11111111111111111111111pubKey49311'), '4Ru'));
test('pubKey2checksum pubKey49311 : 4Ru', t => t.is(app.pubKey2checksum('pubKey49311'), '4Ru'));

test('pubKey2checksum de Nd5...21o:3Q3', t => t.is(app.pubKey2checksum('Nd5kTAZmFDuKoi1mAZkZERenV6efyYyyLoHMTe721o'), '3Q3'));
test('pubKey2checksum simpleSha de Nd5...21o:3Q3', t => t.is(app.pubKey2checksum('Nd5kTAZmFDuKoi1mAZkZERenV6efyYyyLoHMTe721o', false, false, false), 'FCd'));
test('checkKey accept simpleSha pub664777:4fv', t => t.true(app.checkKey('pub664777:4fv', false)));
test('checkKey accept simpleSha pub664777:14f', t => t.true(app.checkKey('pub664777:14f', false)));
test('checkKey accept simpleSha pub664777:2u3', t => t.true(app.checkKey('pub664777:2u3', false)));
test('checkKey accept simpleSha pub664777:12u', t => t.true(app.checkKey('pub664777:12u', false)));

test('pubKey2checksum simpleSha viewDependant Nd5...21o', t => t.is(app.pubKey2checksum('Nd5kTAZmFDuKoi1mAZkZERenV6efyYyyLoHMTe721o', true, false, false), '7G5'));
test('pubKey2checksum simpleSha checksumWithLeadingZero Nd5...21o', t => t.is(app.pubKey2checksum('Nd5kTAZmFDuKoi1mAZkZERenV6efyYyyLoHMTe721o', false, true, false), 'FCd'));
test('pubKey2checksum simpleSha viewDependant checksumWithLeadingZero Nd5...21o', t => t.is(app.pubKey2checksum('Nd5kTAZmFDuKoi1mAZkZERenV6efyYyyLoHMTe721o', true, true, false), '7G5'));


test("isDuniterPubKey verify key size follow duniter specification https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md#public-key",
	(t) => t.true(app.isDuniterPubKey(pubKey)));
test("isDuniterPubKey fail if the key is not in base 58", (t) => t.false(app.isDuniterPubKey(pubKey.replace(/6/,'0'))));
test("isDuniterPubKey fail if the key is to short for duniter specifications", (t) => t.false(app.isDuniterPubKey(pubKey.replace(/F/g,''))));
test("isDuniterPubKey fail if the key is to long for duniter specifications", (t) => t.false(app.isDuniterPubKey(pubKey.replace(/6/,'99'))));
test("isDuniterPubKey fail if the binary key decoded from base 58 is longer than 32bytes", (t) => t.false(app.isDuniterPubKey(Array(44).fill('z').join('') )));
test("isEd25519PubKey verify key validity with ed25519 point decode verification", (t) => t.true(app.isEd25519PubKey(pubKey)));
test("isEd25519PubKey fail if point is not on ed25519", (t) => t.false(app.isEd25519PubKey(pubKey.replace(/6/,'9'))));


test('checkKey accept valid pubKey with no checksum', t => t.true(app.checkKey(pubKey)));
test('checkKey throw if empty pubkey is given', t => t.throws(() => app.checkKey(''),{name:'empty'}));
test('checkKey throw if under_sized string is given', t => t.throws(() => app.checkKey('test'),{name:'too_short'}));
test('checkKey throw too_short if 41 characters long string is given', t => t.throws(() => app.checkKey('fffffffffffffffffffffffffffffffffffffffff'),{name:'too_short'}));
test('checkKey throw too_short if 42 characters long string is given', t => t.throws(() => app.checkKey('ffffffffffffffffffffffffffffffffffffffffff'),{name:'too_short'}));
test('checkKey throw too_long if given string is 44 characters but more than 32bits', t => t.throws(() => app.checkKey('ffffffffffffffffffffffffffffffffffffffffffff'),{name:'too_long'}));
test('checkKey throw if over_sized string is given', t => t.throws(() => app.checkKey(pubKey+pubKey),{name:'too_long'}));
test('checkKey throw too_long if 45 characters long string is given', t => t.throws(() => app.checkKey('fffffffffffffffffffffffffffffffffffffffffffff'),{name:'too_long'}));
test('checkKey throw if invalid pubkey is given (not on ed25519 curve)', t => t.throws(() => app.checkKey(pubKey.replace(/6/,'9')),{name:'bad_ed25519_point'}));
test('checkKey throw if checksum is incorrect', t => t.throws(() => app.checkKey(`${pubKey}:111`),{name:'bad_checksum'}));
test('checkKey throw if not b58 string is given', t => t.throws(() => app.checkKey(`___`),{name:'not_b58'}));

test('checkKey accept valid binary pubKey', t => t.true(app.checkKey(app.b58.decode(pubKey))));

test("isPubKey return true when checkKey return true ", (t) => t.true(app.isPubKey(pubKey)));
test("isPubKey return false when checkKey throw an error", (t) => t.false(app.isPubKey(pubKey.replace(/6/,'9'))));

app.mockRandom((u8a, n)=>{
	if(!u8a && !n) return 0.5;
	if(!n && typeof u8a === "number"){n = u8a;u8a = new Uint8Array(n);}
	for (let i = 0; i < n; i++) u8a[i] = 5;
	return u8a;
});

const user1 = await app.idSecPass2cleanKeys('1','1');
const user2 = await app.idSecPass2cleanKeys('2','2');
const timestampInSeconds = 1222111000;
const sampleMessage = {title:"Mon Titre",content:"Mon message"};
const cryptedMessage =    {
	content: 'G0ZAirsaoeAt/pOcsjg0milDkeBu8Uo3BgrZ',
	hash: '65EC1622D2C38422EA21124397AB7A9F36A94BB6FC2C474C706ECAC33658114C',
	issuer: 'BUhLyJT17bzDVXW66xxfk1F7947vytmwJVadTaWb8sJS',
	nonce: 'TYPjCiGbKiwP6r12cdkmVjySbQpSHavp',
	recipient: '7nge6q7F4k7FQ2q4FRMMPvt2tK7AEx8gNNRLr6LwZN38',
	signature: 'u4m2YHp5nVxcXxMjQvv9H4kfSV8Q3/Jh68jHplbCjizXlA9XXksu+YuFrS9B5KuQ2HX68g9+kRdLoCspMuWtBg==',
	time: timestampInSeconds,
	title: 'yV5cDZCuJWqMBXsFmEm1FSlDkeBX/U02Ag==',
	version: 2
};

test('textEncrypt(jsonMessage, senderPrivateKey, receiverPubKey) encrypt to cesium+ message format',
		t => t.deepEqual(
			app.textEncrypt(sampleMessage, user1.secretKey, user2.publicKey,timestampInSeconds),
			cryptedMessage)
);
test('textDecrypt(jsonMessage, receiverPrivateKey)', t => t.deepEqual(app.textDecrypt(cryptedMessage, user2.secretKey),
	{title:"Mon Titre",content:"Mon message"}));
