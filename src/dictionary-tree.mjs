import {escape2utfSpecial, utfSpecial2escaped, utfSpecial2unEscaped} from "./dictionary-escaper.mjs";

export function buildTreeStruct(monoLineString) {
	const stringAsArray = escape2utfSpecial(monoLineString).split('');
	const rawTree = leftParser(stringAsArray);
	const outOfScope = stringAsArray.length;
	if (outOfScope) throw new Error(`fail to build tree from : "${utfSpecial2escaped(monoLineString)}" parsed: ${JSON.stringify(rawTree)} unparsed/failed: ${utfSpecial2escaped(stringAsArray.join(''))}`);
	let lastTree, tree = rawTree;
	do {
		lastTree = JSON.stringify(tree);
		tree = flattenTree(tree);
	} while (JSON.stringify(tree) !== lastTree);
	preRouteAlt(tree);
	return tree;
}

/**
 * leftParser stuff
 */
function flushNoEmptyString(data) {
	if (data.str.length) data.tree.alt[data.tree.alt.length - 1].step.push({str: data.str});
	data.str = '';
}

function appendToString(chr, data) {
	data.str += chr;
}

function nextAlt(data) {
	data.tree.alt.push({step: []});
}

function goIntoNewAlt(data) {
	data.tree.alt[data.tree.alt.length - 1].step.push(leftParser(data.input));
}

function returnTrue() {
	return true;
}

function doTheses() {
}

const leftParserFunc = {
	')': (chr, data) => returnTrue(flushNoEmptyString(data)),
	'|': (chr, data) => doTheses(flushNoEmptyString(data), nextAlt(data)),
	'(': (chr, data) => doTheses(flushNoEmptyString(data), goIntoNewAlt(data)),
	'default': (chr, data) => appendToString(chr, data)
}

function leftParser(inputChrArray) {
	const data = {
		input: inputChrArray,
		str: '',
		tree: {alt: [{step: []}]},
	};
	while (data.input.length) {
		const chr = data.input.shift();
		if (typeof leftParserFunc[chr] !== 'undefined') {
			const out = leftParserFunc[chr](chr, data);
			if (out) return data.tree;
		} else leftParserFunc['default'](chr, data);
	}
	flushNoEmptyString(data);
	return data.tree;
}

// end leftParser
function concatStrings(array) {
	const data = {
		input: array,
		str: '',
		tree: {alt: [{step: []}]}, // Output array
	};
	array.forEach(e => {
		if (isString(e)) appendToString(e.str, data);
		else {
			flushNoEmptyString(data)
			data.tree.alt[data.tree.alt.length - 1].step.push(e);
		}
	});
	flushNoEmptyString(data)
	return data.tree.alt[data.tree.alt.length - 1].step;
}

function flattenTree(tree) {
	if (isString(tree)) return tree;
	const objType = isAlt(tree) ? 'alt' : 'step';
	tree[objType] = tree[objType].map(flattenTree);
	if (tree[objType].length === 0) return {str: ''};
	if (tree[objType].length === 1) return tree[objType][0];

	if (isAlt(tree)) {
		tree.alt = trivialDedup(tree.alt);
		tree.alt = [].concat(...tree.alt.map(e => isAlt(e) ? e.alt : [e]));
		return tree;
	}
	if (isStep(tree)) {
		tree.step = concatStrings(tree.step);
		return tree;
	}
}

function isString(element) {
	return typeof element === 'object' && typeof element.str === 'string';
}

function isStep(element) {
	return typeof element === 'object' && typeof element.step !== 'undefined';
}

function isAlt(element) {
	return typeof element === 'object' && typeof element.alt !== 'undefined';
}

function trivialDedup(tree) {
	const dedupKeys = {};
	tree.forEach(v => dedupKeys[JSON.stringify(v)] = v); // eslint-disable-line no-return-assign
	return Object.keys(dedupKeys).sort().map(JSON.parse);
}
export function splitAround(pattern,treeStruct){
	function recSplitter(treeStruct){
		if (isString(treeStruct)) {
			if(!treeStruct.str.includes(pattern)) return {notMatching:treeStruct.str};
			if(treeStruct.str.split(pattern).length>2) throw new Error('Error: @@ can only appear once in expression (idSec at left, password à right)');
			return {matching:treeStruct.str};
		}
		if (isStep(treeStruct)) {
			let isMatch = 0;
			let isAlt = false;
			let serialized = '';
			let altSerialized = '';
			treeStruct.step.forEach(subTree=>{
				const subPart = recSplitter(subTree);
				if(subPart.matching) {
					isMatch++;
					if (typeof subPart.notMatching !== 'undefined') isAlt = true;
					serialized+=subPart.matching;
					altSerialized+=subPart.notMatching;
				} else {
					serialized+=subPart.notMatching;
					altSerialized+=subPart.notMatching;
				}
			});
			if(isMatch>1) throw new Error('Error: @@ can only appear once in expression (idSec at left, password à right)');
			if(isMatch && isAlt) return {matching:serialized, notMatching: altSerialized};
			if(isMatch) return {matching:serialized};
			return {notMatching: serialized};
		}
		if (isAlt(treeStruct)) {
			const matching = [];
			const notMatching = [];
			treeStruct.alt.forEach(subTree=>{
				const subPart = recSplitter(subTree);
				if(typeof subPart.matching !== 'undefined') matching.push(subPart.matching);
				if(typeof subPart.notMatching !== 'undefined') notMatching.push(subPart.notMatching);
			})
			if(matching.length && notMatching.length) return {matching: `(${matching.join('|')})`,notMatching: `(${notMatching.join('|')})`}
			if(matching.length) return {matching: `(${matching.join('|')})`}
			if(notMatching.length) return {notMatching: `(${notMatching.join('|')})`}
		}
		throw new Error(`Error: how to splitAround ${pattern} with ${JSON.stringify(treeStruct)}`);
	}
	const res = recSplitter(treeStruct);
	if(typeof res.matching !== 'undefined') res.matching = rawSerialize(buildTreeStruct(res.matching));
	if(typeof res.notMatching !== 'undefined') res.notMatching = rawSerialize(buildTreeStruct(res.notMatching));
	return res;
}
export function serialize(treeStruct) {
	return utfSpecial2escaped(rawSerialize(treeStruct));
}
export function rawSerialize(treeStruct) {
	if (isString(treeStruct)) return treeStruct.str;
	if (isStep(treeStruct)) return treeStruct.step.map(rawSerialize).join('');
	if (isAlt(treeStruct)) return `(${treeStruct.alt.map(rawSerialize).join('|')})`;
	throw new Error(`Error: how to serialize ${JSON.stringify(treeStruct)} RAW: ${treeStruct}`);
}

function preRouteAlt(treeStruct) {
	if (isString(treeStruct)) {
		treeStruct.altCount = 1;
		treeStruct.indexCost = 1;
		return 1;
	}
	if (isAlt(treeStruct)) {
		treeStruct.altCount = treeStruct.alt.reduce((acc, cur) => acc + preRouteAlt(cur), 0);
		treeStruct.indexCost = treeStruct.altCount - 1;
		return treeStruct.altCount;
	}
	if (isStep(treeStruct)) {
		treeStruct.altCount = treeStruct.step.reduce((acc, cur) => acc * preRouteAlt(cur), 1);
		// last alt indexCost*1, previous alt group indexCost * last alt altCount, firstAltGroup * nextAltGroup * nextAltGroup... * lastAltGroup
		let weigth = 1;
		for (let i = treeStruct.step.length - 1; i >= 0; i--) {
			const step = treeStruct.step[i];
			if (isAlt(step)) {
				step.alt.forEach(alt => applyCostWeight(alt, weigth));
				weigth *= step.altCount;
			}
		}
		treeStruct.indexCost = treeStruct.altCount;
		return treeStruct.altCount;
	}
}

function applyCostWeight(tree, weight) {
	tree.indexCost *= weight;
	if (isStep(tree)) tree.step.forEach(subTree => applyCostWeight(subTree, weight));
	if (isAlt(tree)) tree.alt.forEach(subTree => applyCostWeight(subTree, weight));
}

export function altCount(treeStruct) {
	return treeStruct.altCount;
}

export function getAlternative(altIndex, tree) {
	const refAltIndex = {index: altIndex};
	return utfSpecial2unEscaped(_getAlternative(refAltIndex, tree));
}
export function getRawAlternative(altIndex, tree) {
	const refAltIndex = {index: altIndex};
	return _getAlternative(refAltIndex, tree);
}

export function _getAlternative(refAltIndex, tree) {
	if (isString(tree)) return tree.str;
	if (isStep(tree)) {
		return tree.step.map(subTree => _getAlternative(refAltIndex, subTree)).join('');
	}
	if (isAlt(tree)) {
		for (const altTree of tree.alt) {
			// Keep me for debug case
			// Keep console.debug(`In ${serialize(tree)}  ${refAltIndex.index} < ${altTree.indexCost} ${refAltIndex.index < altTree.indexCost?'choose':'avoid'} ${serialize(altTree)}`);
			if (refAltIndex.index < altTree.indexCost) return _getAlternative(refAltIndex, altTree);
			else refAltIndex.index -= altTree.indexCost;
		}
	}
	throw new Error(`index out of bound : ${refAltIndex.index + altCount(tree)} asked, 0-${altCount(tree)-1} available in ${serialize(tree)}`);
}
