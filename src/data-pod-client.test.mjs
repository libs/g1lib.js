import test from 'ava';
import * as app from './data-pod-client.mjs';

test('data-pod-client dummy request', async t => {
	const hosts = ['https://dummy/'];
	const query = 'user/profile/2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT?&_source=title';
	const expectedResult = JSON.parse(`{
		"_index":"user","_type":"profile","_id":"2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT","_version":11,
		"found":true,
		"_source":{"title":"[1000i100] Millicent BILLETTE"}
	}`);
	app.mockableDeps.fetch = str => { return {json: () => expectedResult}; }; // eslint-disable-line no-unused-vars
	const client = new app.DataPodClient(hosts);
	const result = await client.query(query);

	t.is(result._source.title, expectedResult._source.title);
});
