import {buildTreeStruct, getRawAlternative, serialize} from "./dictionary-tree.mjs";
import {parse} from "./dictionary-parser.mjs";
import {utfSpecial2unEscaped} from "./dictionary-escaper.mjs";

export class Dictionary {
	constructor(dictionaryString, options= {}) {
		this.originalConfig = options;
		this.originalConfig.dictionaryString = dictionaryString;
		this.config = JSON.parse(JSON.stringify(this.originalConfig));
		if(typeof this.config.cache === 'undefined') this.config.cache = true;
		if(typeof this.config.cacheMax === 'undefined') this.config.cacheMax = 1_000_000;
		if(typeof this.config.idSecPwd === 'undefined') this.config.idSecPwd = true;
		if(this.originalConfig.escapeAll !== 1) try{
			this.config.escapeAll = 0;
			this.tree = buildTreeStruct(parse(dictionaryString,this.config));
			this.config.escapeAll = this.originalConfig.escapeAll;
		} catch (e){
			this.config.escapeAll = 1;
		}
		this.tree = buildTreeStruct(parse(dictionaryString,this.config));
		this.length = this.tree.altCount;
		this.estimateDuration = ()=>this.length/(this.config.speed || 1)
		this.estimateRemaining = ()=>(this.length-this.tried)/(this.config.speed || 1)
		if(this.config.speed) adjustVariant(this);
		this.tried = 0;
		this.cache = [];
		this.duplicatedCount = ()=> Object.keys(this.cache).length? this.tried - Object.keys(this.cache).length : 0;
		this.dryGet = index => utfSpecial2unEscaped(this.rawDryGet(index));
		this.rawDryGet = index => getRawAlternative(index,this.tree);
		this.get = index => utfSpecial2unEscaped(this.rawGet(index));
		this.rawGet = index => {
			if(typeof this.startTime === "undefined") this.startTime = Date.now();
			this.tried++;
			const alt = getRawAlternative(index,this.tree);
			if(this.config.cache && this.length < this.config.cacheMax) {
				if(typeof this.cache[alt] === "undefined") this.cache[alt] = [];
				this.cache[alt].push(index);
				if(this.cache[alt].length>1) return `§duplicate§${alt}`;
			}
			return alt;
		}
		function _split(str){
			const parts = str.split('@@');
			const idSec = utfSpecial2unEscaped(parts[0]);
			const pwd = utfSpecial2unEscaped(parts[parts.length - 1]);
			const res = [idSec,pwd];
			res.idSec = idSec;
			res.pwd = pwd;
			res.pass = pwd;
			return res;
		}
		this.splitGet = index => _split(this.rawGet(index));
		this.splitDryGet = index => _split(this.rawDryGet(index));
		this.timeSpent = ()=>(Date.now()-this.startTime)/1000;
		this.duplicatedFound = ()=>{
			const duplicated = [];
			for(let key of Object.keys(this.cache)) if(this.cache[key].length>1) duplicated.push({alt:key,index:this.cache[key]});
			const sortedDuplicate = duplicated.sort((a,b)=>b.index.length-a.index.length);
			return sortedDuplicate;
		}
		this.dryRunDedup = ()=>dryRunDedup(this);
		this.serialize = ()=>serialize(this.tree);
	}
}
function isAuto(evaluable){
	return evaluable === -1 || typeof evaluable === 'undefined' || evaluable === "auto" || evaluable === '-1';
}
function adjustVariant(self){
	const durationGoal = 3_600;
	if (self.estimateDuration() >= durationGoal) return;
	if (self.length >= self.config.cacheMax) return;
	function rebuildTree(self){
		self.tree = buildTreeStruct(parse(self.originalConfig.dictionaryString,self.config));
		self.length = self.tree.altCount;
	}
	let lastLess1hConfig;
	while (self.estimateDuration() < durationGoal && self.length < self.config.cacheMax){
		//console.log(`${parseInt(self.estimateDuration())}s, alt:${self.length} \\:${self.config.escapeAll} à:${self.config.accent} M:${self.config.lowerCase} 3:${self.config.leetSpeak}`, serialize(self.tree));
		lastLess1hConfig = JSON.parse(JSON.stringify(self.config));
		if(self.config.escapeAll !== 2 && isAuto(self.originalConfig.escapeAll)) {
			self.config.escapeAll = 2;
			rebuildTree(self);
			continue;
		}
		if(self.config.accent !== 2 && isAuto(self.originalConfig.accent)) {
			if(!self.config.accent) self.config.accent = 1;
			else self.config.accent++;
			rebuildTree(self);
			continue;
		}
		if(self.config.lowerCase !== 2 && isAuto(self.originalConfig.lowerCase)) {
			if(!self.config.lowerCase) self.config.lowerCase = 1;
			else self.config.lowerCase++;
			rebuildTree(self);
			continue;
		}
		if(self.config.leetSpeak !== 3 && isAuto(self.originalConfig.leetSpeak)) {
			if(!self.config.leetSpeak) self.config.leetSpeak = 1;
			else self.config.leetSpeak++;
			rebuildTree(self);
			continue;
		}
		break;
	}
	self.config = lastLess1hConfig || self.config;
	rebuildTree(self);
	dryRunDedup(self);
}
function dryRunDedup(self){
	const dry = new Dictionary(serialize(self.tree),{cache:true,idSecPwd:false});
	for(let i = 0; i < dry.length;i++) dry.get(i);
	const duplicate = dry.duplicatedFound();
	self.duplicateTotal = dry.duplicatedCount();
	self.duplicateRatio = self.duplicateTotal/self.length;
	self.uniqueRatio = 1 - self.duplicateRatio;
	return duplicate;
}
