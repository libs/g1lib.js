import test from 'ava';
import * as app from './basex.mjs';

// Base58
const pubKey = 'AoxVA41dGL2s4ogMNdbCw3FFYjFo5FPK36LuiW1tjGbG';

test('b58 should decode/encode well', t => t.is(app.b58.encode(app.b58.decode(pubKey)), pubKey));
test('basex dont allow ambigous alphabet (each character must be unique)', t => t.throws(() => app.basex('zz')));
test('empty input empty output', t => t.is(app.b58.encode([]), ''));
test('decode out of base chr throw error', t => t.throws(() => app.b58.decode(pubKey + '§')));
test('no string decode throw', t => t.throws(() => app.b58.decode([])));

test('encode 0000 filled source', t => t.is(app.b16.encode([0, 0, 0, 0, 15]), '0000f'));
test('decode 0000 filled source', t => t.deepEqual(app.b16.decode('0000f'), new Uint8Array([0, 0, 0, 0, 15])));
test('decode empty string => empty array', t => t.deepEqual(app.b16.decode(''), new Uint8Array(0)));

test('b64 should encode Man as TWFu', t => t.is(app.b64.encode('Man'), 'TWFu'));
test('b64 should encode Ma as TWE=', t => t.is(app.b64.encode('Ma'), 'TWE='));
test('b64 should encode M as TQ==', t => t.is(app.b64.encode('M'), 'TQ=='));
test('b64 should decode TWFu as Man', t => t.is((new TextDecoder()).decode(app.b64.decode('TWFu')), 'Man'));
test('b64 should decode TWE= as Ma', t => t.is((new TextDecoder()).decode(app.b64.decode('TWE=')), 'Ma'));
// Won't fix test('b64 should decode TWE as Ma', t => t.is((new TextDecoder()).decode(app.b64.decode('TWE')), 'Ma'));
test('b64 should decode TQ== as M', t => t.is((new TextDecoder()).decode(app.b64.decode('TQ==')), 'M'));
// Won't fix test('b64 should decode TQ as M', t => t.is((new TextDecoder()).decode(app.b64.decode('TQ')), 'M'));


test('[0,5,5] should be encoded as AAUF', t => t.is(app.b64.encode([0,5,5]), 'AAUF'));
test('AAUF should be decoded as [0,5,5]', t => t.deepEqual(app.b64.decode('AAUF'), new Uint8Array([0,5,5])));
test('[0,1] should be encoded as AAE=', t => t.is(app.b64.encode([0,1]), 'AAE='));
test('AAE= should be decoded as [0,1]', t => t.deepEqual(app.b64.decode('AAE='), new Uint8Array([0,1])));


//const unstableExample = 'AUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBVCmFAmx3dAyXpsWtwDnGel3LAcACxvXeG6QfGU9IEld9WwlvkDttnDTPgQL6TQkiMl0SCo=';
const unstableExample =  'AAUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBVCmFAmx3dAyXpsWtwDnGel3LAcACxvXeG6QfGU9IEld9WwlvkDttnDTPgQL6TQkiMl0SCo=';
test('b64 should be stable in decoding encoding process even on long string', t => t.is(app.b64.encode(app.b64.decode(unstableExample)), unstableExample));
