import test from 'ava';
import * as app from './dictionary.mjs';

function sleep(ms){
	return new Promise((resolve)=>setTimeout(resolve,ms));
}
test('get dictionary length', t => {
	const dictionaryString = '(a|b|c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString,{idSecPwd:false});
	t.is(dico.length, 9);
});
test('get dictionary iteration', t => {
	const dictionaryString = '(a|b|c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString);
	t.is(dico.get(5), "ade@@bdg");
});
test('get iteration are tracked', t => {
	const dico = new app.Dictionary('(a|b|c)d(e|f|g)');
	t.is(dico.tried, 0);
	dico.get(1);
	dico.get(2);
	t.is(dico.tried, 2);
});
test('dryGet iteration are not tracked', t => {
	const dico = new app.Dictionary('(a|b|c)d(e|f|g)');
	t.is(dico.tried, 0);
	dico.dryGet(1);
	dico.dryGet(2);
	t.is(dico.tried, 0);
});
test('_\\@\\@_@@_@\\@_ can be ambiguous with get, dryGet or not with rawGet, rawDryGet, splitGet', t => {
	const dico = new app.Dictionary('(\\@\\@|_@@\\)@\\@)', {cache:false});
	t.is(dico.length, 2);
	t.is(dico.dryGet(0), '_@@)@@');
	t.is(dico.dryGet(1), '@@@@@@');
	t.is(dico.get(1), '@@@@@@');
	const escAro = String.fromCharCode(0x230d);
	t.is(dico.rawGet(0), `_@@${String.fromCharCode(0x2301)}@${escAro}`);
	t.is(dico.rawDryGet(1), `${escAro+escAro}@@${escAro+escAro}`);
	t.is(dico.splitGet(0)[0], '_');
	t.is(dico.splitGet(0)[1], ')@@');
	t.is(dico.splitGet(0).idSec, '_');
	t.is(dico.splitGet(0).pwd, ')@@');
	t.is(dico.splitGet(0).pass, ')@@');
	t.is(dico.splitDryGet(0)[0], '_');
	t.is(dico.splitDryGet(0)[1], ')@@');
});
test('\\{\\} work fine', t => {
	const dico = new app.Dictionary('\\{\\}');
	t.is(dico.length, 1);
	t.is(dico.serialize(), '\\{\\}@@\\{\\}');
	t.is(dico.dryGet(0), '{}@@{}');
});
test('8\\<Z work fine', t => {
	const dico = new app.Dictionary('8\\<Z');
	//t.is(dico.length, 1);
	t.is(dico.serialize(), '8\\<Z@@8\\<Z');
	t.is(dico.dryGet(0), '8<Z@@8<Z');
});
test('get is time tracked', async t => {
	const dico = new app.Dictionary('(a|b|c)d(e|f|g)');
	dico.get(1);
	await sleep(5);
	dico.get(2);
	t.true(dico.timeSpent()>=0.005);
});
test('estimateDuration && estimateRemaining', t => {
	const dico = new app.Dictionary('(a|b|c)d(e|f|g)');
	dico.get(1);
	dico.get(2);
	t.is(dico.estimateDuration(), 81);
	t.is(dico.estimateRemaining(), 79);
});
test('get duplicated found count (from dictionary)', t => {
	const dictionaryString = '(a|b|c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString);
	dico.get(0);
	dico.get(1);
	t.is(dico.duplicatedCount(), 0);
});
test('get duplicated found in past iteration (from dictionary)', t => {
	const dictionaryString = '((a|b)cd|(a|b)c(d|e)|bc(d|e))';
	const dico = new app.Dictionary(dictionaryString, {idSecPwd:false});
	for(let i = 0; i < dico.length;i++) dico.get(i);
	t.deepEqual(dico.duplicatedFound(), [ {alt:"bcd",index: [ 2, 5, 6 ]}, {alt:"acd",index: [ 0, 4 ]}, {alt:"bce",index: [ 3, 7 ]} ]);
});
test('dictionary can dryRun to find all duplicate', t => {
	const dictionaryString = '((a|b)cd|(a|b)c(d|e)|bc(d|e))';
	const dico = new app.Dictionary(dictionaryString, {idSecPwd:false});
	const duplicate = dico.dryRunDedup();
	t.deepEqual(duplicate, [ {alt:"bcd",index: [ 2, 5, 6 ]}, {alt:"acd",index: [ 0, 4 ]}, {alt:"bce",index: [ 3, 7 ]} ]);
	t.is(dico.length,8);
	t.is(dico.duplicateTotal,4);
});

test('dictionary can run with cache disabled', t => {
	const dictionaryString = '(a|b|c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString,{cache:false});
	dico.get(0);
	t.is(Object.keys(dico.cache).length, 0);
});
test('dictionary can run with cache on', t => {
	const dictionaryString = 'a';
	const dico = new app.Dictionary(dictionaryString,{cache:true});
	dico.get(0);
	dico.get(0);
	t.is(dico.duplicatedCount(), 1);
});
test('duplicate match §duplicate§ pattern', t => {
	const dictionaryString = 'a';
	const dico = new app.Dictionary(dictionaryString);
	const first = dico.get(0);
	const second = dico.get(0);
	t.is(first, 'a@@a');
	t.is(second, '§duplicate§a@@a');
});
test('dictionary called with speed option try to activate variante accent, caps and leetSpeak option to reach 1h of compute estimated time', t => {
	const dictionaryString = 'Ǧ3Ǧo';
	const dico = new app.Dictionary(dictionaryString, {speed:30});
	t.is(dico.length,2025);
	t.is(dico.duplicateTotal,729);
	t.is(dico.duplicateRatio.toPrecision(2),'0.36');
	t.is(dico.uniqueRatio.toPrecision(2),'0.64');
});
test('auto activate leetSpeak for tiny case', t => {
	const dictionaryString = 'a';
	const dico = new app.Dictionary(dictionaryString, {speed:30});
	t.is(dico.serialize(),'(4|A|a)@@(4|A|a)');
	t.is(dico.length,9);
});
test('dico should not crash', t=>{
	const v8CrashStringButFirefoxWork = '(((Ǧ|ǧ)|(G|g))(1|i|l|I)((Ǧ|ǧ)|(G|g))(1|i|l|I)@@((Ǧ|ǧ)|(G|g))(1|i|l|I)((Ǧ|ǧ)|(G|g))(1|i|l|I)|(((Ǧ|ǧ)|(G|g))(1|i|l|I)((Ǧ|ǧ)|(G|g))(1|i|l|I)@@((Ǧ|ǧ)|(G|g))(1|i|l|I)((Ǧ|ǧ)|(G|g))(1|i|l|I)|(ǧ|g)(1|i|l|I)(ǧ|g)(1|i|l|I)@@(ǧ|g)(1|i|l|I)(ǧ|g)(1|i|l|I)))';
	const dico = new app.Dictionary(v8CrashStringButFirefoxWork,{speed:30})
	t.is(dico.length,73988);
	t.is(dico.duplicateTotal,8448);
	t.is(dico.duplicateRatio.toFixed(3),'0.114');
	t.is(dico.uniqueRatio.toFixed(3),'0.886');
	t.is(dico.estimateDuration().toFixed(0),'2466');
});
test('just under 1h : try to find variant', t => {
	const dictionaryString = 'onlyOne@@[a-z0-8]';
	const dico = new app.Dictionary(dictionaryString, {speed:0.01});
	t.is(dico.length,35);
	t.is(dico.duplicateTotal,0);
});
test("just over 1h : don't search for variants", t => {
	const dictionaryString = 'onlyOne@@[a-z0-9 ]';
	const dico = new app.Dictionary(dictionaryString, {speed:0.01});
	t.is(dico.length,37);
	t.is(typeof dico.duplicateTotal, 'undefined');
});
test("alt number >= 1_000_000 disable cache and variants attempt", t => {
	const dico = new app.Dictionary(`[0-9]{6}@@onlyOne`, {speed:1_000_000});
	t.is(dico.length,1_000_000);
	t.is(typeof dico.duplicateTotal, 'undefined');
});
test("huge alt number work fine", t => {
	const x = 12;
	const dico = new app.Dictionary(`[0-9]{${2+x}}@@[a-z0-9 ]`, {speed: 10**x});
	t.is(dico.length,3_700 * Math.pow(10,x));
	t.is(typeof dico.duplicateTotal, 'undefined');
});
test("huge alt number apply variants if set", t => {
	const x = 12;
	t.is((new app.Dictionary(`[0-9]{${2+x}}@@[A-Z]`, {lowerCase:0})).length,2_600 * 10**x);
	t.is((new app.Dictionary(`[0-9]{${2+x}}@@[A-Z]`, {lowerCase:1})).length,5_200 * 10**x);
});
test('escaped special characters still escaped when re-serialized', t => {
	const dictionaryString = '(a|b|c\\)c)d(e|f|g)';
	const dico = new app.Dictionary(dictionaryString, {idSecPwd:false});
	const serialized = dico.serialize();
	t.is(serialized, dictionaryString);
	t.is(dico.get(6), 'c)cde');
});
test('escapeAll:0 -> regex parse strings', t => {
	const dico = new app.Dictionary('[0-1]{3}@@=autre>\nautre::ça',{escapeAll:0});
	t.is(dico.length, 8);
});
test('escapeAll:1 -> everything is escaped', t => {
	const dico = new app.Dictionary('[0-1]{3}@@=autre>\nautre::ça',{escapeAll:1});
	t.is(dico.length, 4);
});
test('escapeAll:2 -> everything is escaped', t => {
	const dico = new app.Dictionary('[0-1]{3}@@=autre>\nautre::ça',{escapeAll:2});
	t.is(dico.length, 12);
});
test('escapeAll:auto -> try 2 if too slow 0 if previous fail 1', t => {
	const dico = new app.Dictionary('[0-1]{3}@@=autre>\nautre::ça',{escapeAll:'auto',speed:30});
	t.is(dico.length, 537);
});
test('escapeAll:undefined -> like auto', t => {
	const dico = new app.Dictionary('[0-1]{3}@@=autre>\nautre::ça',{speed:30});
	t.is(dico.length, 537);
});
test('invalid string -> autofallback to escapeAll:1', t => {
	const dico = new app.Dictionary('[0-1]{z}@@=autre>\nautre::ça');
	t.is(dico.length, 4);
});
test(`"aa" with default settings & a speed should generate alt including "Aa@@AA"`, t => {
	const dico = new app.Dictionary('aa',{speed:30, leetSpeak:0});
	for(let i=0;i<dico.length;i++) dico.splitGet(i);
	t.is(dico.cache['Aa@@AA'][0], 2);
});
test(`"(|a)" with idSecPwd:true should have 4 cases`, t => {
	const dico = new app.Dictionary('(|a)',{idSecPwd:true});
	t.is(dico.length, 4);
});
