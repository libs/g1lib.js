export async function timeTrack(arrayFuncAndParams,timeout){
	return Promise.race([
		new Promise(resolve => setTimeout(
				()=>resolve({ms:timeout,params:arrayFuncAndParams,timeout:true}),
				timeout
			)),
		new Promise(async resolve=>{
			const startTime = Date.now();
			const func = arrayFuncAndParams[0];
			const params = arrayFuncAndParams.slice(1);

			const res = await func(...params);
			const endTime = Date.now();
			const elapsed = endTime - startTime;
			resolve({ms:elapsed,params:arrayFuncAndParams,result:res});
		})]
	);
}
