import test from 'ava';
import * as app from './dictionary-tree.mjs';

const buildTreeThenSerialize = str => app.serialize(app.buildTreeStruct(str));

test('simple string still simple string', t => t.is(buildTreeThenSerialize('abc'), 'abc'));
test('(a|b) alt still (a|b)', t => t.is(buildTreeThenSerialize('(a|b)'), '(a|b)'));
test('a)b throw', t => t.throws(() => app.buildTreeStruct('a)b')));
// Ok to be permissive test('(a throw',t=>t.throws(()=>buildTreeStruct('(a')));
// Ok to be permissive test('a|b throw',t=>t.throws(()=>buildTreeStruct('a|b')));
test('(|b) keep empty choice', t => t.is(buildTreeThenSerialize('(|b)'), '(|b)'));
test('(b|b) trivial dedup', t => t.is(buildTreeThenSerialize('(|b||b|)'), '(|b)'));
test('a(b|c) mix fix and alt', t => t.is(buildTreeThenSerialize('a(b|c)'), 'a(b|c)'));
test('a(b) flat merge when no alt', t => t.is(buildTreeThenSerialize('a(b)'), 'ab'));
test('(a(b|c)|(d|e)) flat merge when unneeded depth', t => t.is(buildTreeThenSerialize('(a(b|c)|(d|e))'), '(a(b|c)|d|e)'));
test('build complexe tree with (|) pattern', t => t.is(buildTreeThenSerialize('(a(|b@@c|d|)|(e|f)|g|h@@i)'), '(a(|b@@c|d)|e|f|g|h@@i)'));

test('serialize incorrect tree throw', t => t.throws(() => app.serialize({plop: ['a']})));

test('splitAround throw when to many split', t => t.throws(() => app.splitAround('@@',app.buildTreeStruct('z@@a(b|c@@d)'))));
test('splitAround throw with @@@@', t => t.throws(() => app.splitAround('@@',app.buildTreeStruct('@@@@'))));
test('splitAround throw with invalid tree', t => t.throws(() => app.splitAround('@@',[])));
test('splitAround return notMatching case', t => t.deepEqual(app.splitAround('@',app.buildTreeStruct('a(b|c)')),{notMatching:'a(b|c)'}));
test('splitAround return matching case', t => t.deepEqual(app.splitAround('@',app.buildTreeStruct('a@b')),{matching:'a@b'}));
test('splitAround return both matching case and not matching one', t => t.deepEqual(app.splitAround('@',app.buildTreeStruct('a@b|c')),{matching:'a@b',notMatching:'c'}));

test('mono altCount', t => t.is(app.altCount(app.buildTreeStruct('ipsum')), 1));
test('simple altCount', t => t.is(app.altCount(app.buildTreeStruct('(lore|ipsu)m')), 2));
test('multi altCount', t => t.is(app.altCount(app.buildTreeStruct('(a|b|c)(d|e|f)g(h|i|j|k)')), 36));
test('multi level tree altCount', t => t.is(app.altCount(app.buildTreeStruct('a(b(c|d)|e(f|g|h)ij(k|l)|@@m)')), 9));

const exampleTree = () => app.buildTreeStruct('a((b|c)d|e(f|g)|h(i(j|k)|l|m)no(p(q|r)|s(t|u)|v|w))');
// console.log(JSON.stringify(exampleTree()));
// console.log(app.serialize(exampleTree()));
/* exampleTree detailed

a( 0-27
  (b|c)d 0-1
 |e(f|g) 2-3
 |h( 4-27
    i(j|k) 4-9 & 10-15
   |l 16-21
   |m 22-27
  )no( 4-27
    p(q|r) 4-5 & 10-11 & 16-17 & 22-23
   |s(t|u) 6-7 & 12-13 & 18-19 & 24-25
   |v 8 & 14 & 20 & 26
   |w 9 & 15 & 21 & 27
  )
)
 */
test('getAlternative 0', t => t.is(app.getAlternative(0, exampleTree()), 'abd'));
test('getAlternative 1', t => t.is(app.getAlternative(1, exampleTree()), 'acd'));
test('getAlternative 2', t => t.is(app.getAlternative(2, exampleTree()), 'aef'));
test('getAlternative 3', t => t.is(app.getAlternative(3, exampleTree()), 'aeg'));
test('getAlternative 4', t => t.is(app.getAlternative(4, exampleTree()), 'ahijnopq'));
test('getAlternative 5', t => t.is(app.getAlternative(5, exampleTree()), 'ahijnopr'));
test('getAlternative 6', t => t.is(app.getAlternative(6, exampleTree()), 'ahijnost'));
test('getAlternative 7', t => t.is(app.getAlternative(7, exampleTree()), 'ahijnosu'));
test('getAlternative 8', t => t.is(app.getAlternative(8, exampleTree()), 'ahijnov'));
test('getAlternative 9', t => t.is(app.getAlternative(9, exampleTree()), 'ahijnow'));
test('getAlternative 10', t => t.is(app.getAlternative(10, exampleTree()), 'ahiknopq'));
test('getAlternative 15', t => t.is(app.getAlternative(15, exampleTree()), 'ahiknow'));
test('getAlternative 16', t => t.is(app.getAlternative(16, exampleTree()), 'ahlnopq'));
test('getAlternative 27', t => t.is(app.getAlternative(27, exampleTree()), 'ahmnow'));
test('getAlternative 28 or more throw', t => t.throws(() => app.getAlternative(28, exampleTree())));

test('escaped special characters are reconverted with getAlternative but not with getRawAlternative', t => {
	const tree = app.buildTreeStruct('a\\(b(c|d)@@e');
	t.is(app.getAlternative(0,tree), 'a(bc@@e');
	t.is(app.getRawAlternative(0,tree), `a${String.fromCharCode(0x2300)}bc@@e`);
	const treeWhereRawIsUseful = app.buildTreeStruct('a\\@\\@b(c|d)@@e');
	t.is(app.getAlternative(0,treeWhereRawIsUseful), 'a@@bc@@e');
	const escAro = String.fromCharCode(0x230d);
	t.is(app.getRawAlternative(0,treeWhereRawIsUseful), `a${escAro+escAro}bc@@e`);
});
